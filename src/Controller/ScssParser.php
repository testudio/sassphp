<?php

namespace SassPhp\Controller;

class ScssParser {

  protected $sass;
  protected $variables;
  protected $strcutures;

  function __construct($sass) {
    $this->sass = $sass;
  }

  function parse() {

    $newScss = [];

    $this->morphNewLinePerCommand();
    $this->morphTrimLines();
    $this->morphRemoveEmptyLines();
    $this->verifyParenthesis();
    $this->processNesting();

    $arraySplit = explode("\n", $this->sass);
    foreach ($arraySplit as $index => $line) {

      // Variable processing.
      if (preg_match('/^(\$.*?)\s*:\s*(.*?);$/is', $line, $matches)) {
        $variablename = $matches[1];
        $variablevalue = $matches[2];

        $this->variables[$variablename] = $variablevalue;

        // var_dump($matches);

      }
      else {
        // Check validity of the line.
        $newScss[] = $line;
      }

    }

    $this->sass = implode("\n", $newScss);
    // If varibales detected.
    if ($this->variables) {
      foreach ($this->variables as $name => $value) {
        $this->sass = str_replace($name, $value, $this->sass);
      }
    }

    return $this->sass;
  }

  function morphNewLinePerCommand() {
    $this->sass = str_replace(";", ";\n", $this->sass);
    $this->sass = str_replace("{", "\n{\n", $this->sass);
    $this->sass = str_replace("}", "\n}\n", $this->sass);
  }

  function morphRemoveEmptyLines() {
    $this->sass = explode("\n", $this->sass);
    $this->sass = array_filter($this->sass, function($value, $key) {
      return (!empty($value));// || ($value !== "\r");
    }, ARRAY_FILTER_USE_BOTH);
    $this->sass = implode("\n", $this->sass);
  }

  function morphTrimLines() {
    $this->sass = explode("\n", $this->sass);
    $this->sass = array_map('trim', $this->sass);
    $this->sass = implode("\n", $this->sass);
  }

  function verifyParenthesis() {
    $values = explode("\n", $this->sass);
    $valueCounts = array_count_values($values);
    if ((isset($valueCounts['{']) || (isset($valueCounts['}']))) &&
      ($valueCounts['{'] !== $valueCounts['}'])) {
      throw new \Exception('Parsing failed: parenthesis is not matching.');
    }
  }

  function processNesting() {
    $selectors = [];
    $structs = [];
    $this->sass = explode("\n", $this->sass);
    foreach ($this->sass as $line) {
      // Process current selector.
      if (! in_array(substr($line, -1), ['{', '}', ';'])) {
        $selectors[] = $line;
      }
      elseif (substr($line, -1) == '}') {
        array_pop($selectors);
      }

      // Process structure.
      if (substr($line, -1) == ';') {
        $structs[implode(' ', $selectors)][] = $line;
      }
    }

    $this->sass = "";
    foreach ($structs as $selector => $struct) {
      $this->sass .= $selector . "\n{\n" . implode("\n", $struct) . "\n}\n";
    }

  }

}
