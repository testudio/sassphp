<?php

namespace SassPhp\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SassPhp\Controller\ScssParser;

class ScssCompile extends Command {

  protected function configure() {
    $this->setName('sass:compile')
      ->setDescription('Compiles SASS code into CSS')
      ->setHelp('This command compiles SASS code into CSS.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $now = date('c');

    // First example.
    /*$scss = '$font-stack:    Helvetica, sans-serif;
$primary-color: #333;$secondary-color: #666;

body {
  font: 100% $font-stack;
  color: $primary-color;  background-color: $primary-color;
}';*/


    // Second example.
    $scss = 'nav {
  ul {
    margin:   0;
    padding: 0;
    list-style: none;
  }

  li { display: inline-block; }

  a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
}';

    $parser = new ScssParser($scss);
    $message = sprintf("Before/After:\n============\n%s\n============\n%s", $scss, $parser->parse());

    $output->writeln($message);

  }

}
