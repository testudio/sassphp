<?php

require __DIR__ . '/vendor/autoload.php';

use SassPhp\Command\ScssCompile;
use Symfony\Component\Console\Application;

$app = new Application();

$app->add(new ScssCompile());

$app->run();
